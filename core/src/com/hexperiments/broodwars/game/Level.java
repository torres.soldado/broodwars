package com.hexperiments.broodwars.game;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.hexperiments.broodwars.game.objects.Brood;
import com.hexperiments.broodwars.map.Dijkstra;
import com.hexperiments.broodwars.map.GameMapData;
import com.hexperiments.broodwars.map.Node;
import com.hexperiments.broodwars.utils.Constants;

public class Level {
	public static final String TAG = Level.class.getName();
	
	GameMapData map = new GameMapData();
	public Brood brood;
	public Brood brood2;
	Thread broodT;
	Thread brood2T;
	Thread pathT;
	Dijkstra pathFinder;
	
	public Level() {
		map.loadMap(Constants.LEVEL_01);
		brood = new Brood(map, 10, 10, 10000, 1.0f, 0.5f, 0.5f);
		brood2 = new Brood(map, 200, 200, 10000, 0.5f, 0.5f, 1.0f);
		broodT = new Thread(brood, "Brood1");
		brood2T = new Thread(brood2, "Brood2");
		pathFinder = new Dijkstra(map.nodes, map.cells);
		pathT = new Thread(pathFinder, "PathFinder");
//		pathFinder.getPathsTo(200, 200);
		broodT.start();
		brood2T.start();
		pathT.start();
	}
	
	public void render(SpriteBatch batch) {
		// draw background
		batch.draw(Assets.instance.levels.level01, 0, 0);
		
		// debug: draw nodes
		if (Constants.DEBUG_ENABLED) {
			BitmapFont font = Assets.instance.fonts.defaultNormal;
			font.setColor(0, 0, 1, 1);
			for (Node node: map.nodes) {
				batch.setColor(1.0f, 1.0f, 1.0f, (node.distance / (float) pathFinder.lastDistance) * 1.0f);
				batch.draw(Assets.instance.solidColors.black, node.x, node.y,	node.w, node.h);
				font.draw(batch, "" + node.distance, node.centerX - 5, node.centerY - 5);
			}
			batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		}
		
		// draw broods
		brood.render(batch);
		brood2.render(batch);
	}
	
	float timePathFinder = 0;
	public void update(float deltaTime) {
//		brood.update(deltaTime);
//		timePathFinder += deltaTime;
//		if (timePathFinder >= 0.05f) {
//			timePathFinder = 0;
//			pathFinder.getPathsTo(brood.targX, brood.targY);
//		}
	}
	
	public void setTargetCoordinates(int x, int y) {
		if (map.cells[x][y] != GameMapData.CELL_INVALID) {
			brood.setTargetCoordinates(x, y);
			brood2.setTargetCoordinates(x, y);
			pathFinder.targX = x;
			pathFinder.targY = y;
		}
	}
}
