package com.hexperiments.broodwars.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.hexperiments.broodwars.utils.Constants;
import com.hexperiments.broodwars.utils.GamePreferences;

public class WorldRenderer implements Disposable 	{
	@SuppressWarnings("unused")
	private static final String TAG = WorldRenderer.class.getName();
	
	private OrthographicCamera cameraGUI; // Fixed to viewport dimensions, to render GUI components.
	private OrthographicCamera camera; // World camera.
	private SpriteBatch batch;
	private WorldController worldControllerRef;
	
	public WorldRenderer(WorldController worldController) {
		this.worldControllerRef = worldController;
		init();
	}
	
	private void init() {
		cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
		cameraGUI.position.set(0, 0, 0);
		cameraGUI.setToOrtho(true); // Flip y-axis
		cameraGUI.update();
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		camera.position.set(0, 0, 0);
		camera.update();
		
		// Pass the world camera reference to the controller so it can properly translate the input touch 
		// coordinates.
		worldControllerRef.setWorldCamera(camera);
	}
	
	private void renderWorld(SpriteBatch batch) {
		worldControllerRef.cameraHelper.applyTo(camera);
		batch.setProjectionMatrix(camera.combined);
		
		batch.begin();
		
		worldControllerRef.level.render(batch);
		
		batch.end();
	}
	
	private void renderGUIFPSCounter(SpriteBatch batch) {
		float x = cameraGUI.viewportWidth - 55;
		float y = cameraGUI.viewportHeight - 15;
		int fps = Gdx.graphics.getFramesPerSecond();
		BitmapFont fpsFont = Assets.instance.fonts.defaultNormal;
		if (fps >= 45) {
			fpsFont.setColor(0, 1, 0, 1); // 45 or more fps show up in green
		} else if (fps >= 30) {
			fpsFont.setColor(1, 1, 0, 1); // 30 or more fps show up in yellow
		} else {
			fpsFont.setColor(1, 0, 0, 1); // less than 30 fps show up in red
		}
		fpsFont.draw(batch, "FPS: " + fps, x, y);
		fpsFont.setColor(1, 1, 1, 1); // Reset to white
	}
	
	private void renderGUI(SpriteBatch batch) {
		batch.setProjectionMatrix(cameraGUI.combined);
		batch.begin();
		
		// Draw FPS text (bottom right edge)
		if (GamePreferences.instance.showFPSCounter) renderGUIFPSCounter(batch);
		
		batch.end();
	}
	
	public void render() {
		renderWorld(batch);
		renderGUI(batch);
	}
	
	public void resize(int width, int height) {
		// Scale GUI
		cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
		cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / (float)height) * (float)width;
		cameraGUI.position.set(cameraGUI.viewportWidth / 2, cameraGUI.viewportHeight / 2, 0);
		cameraGUI.update();
		
		// Scale viewport width keeping the world's visible height constant
		camera.viewportWidth = (Constants.VIEWPORT_HEIGHT / (float)height) * (float)width;
		camera.update();
	}

	@Override
	public void dispose() {
		batch.dispose();
	}
}
