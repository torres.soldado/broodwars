package com.hexperiments.broodwars.map;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.hexperiments.broodwars.map.FibonacciHeap.Entry;
import com.hexperiments.broodwars.utils.Constants;

public class Dijkstra implements Runnable {
	public static final String TAG = Dijkstra.class.getName();
	private ArrayList<Node> nodesRef; // ToDo: Use simple array or trove arraylist
	int[][] cellsRef;
	int tag;
	FibonacciHeap q, q2, curQ, altQ;
	boolean curQisQ;
	public int lastDistance = 0;
	public int targX;
	public int targY;
	
	public Dijkstra(ArrayList<Node> nodes, int[][] cells) {
		this.nodesRef = nodes;
		this.cellsRef = cells;
		tag = 0;
		q = new FibonacciHeap();
		q2 = new FibonacciHeap();
		
		for (int i = 0; i < nodes.size(); ++i) {
			Node node = nodes.get(i);
			q.enqueue(node, 1000000);
			curQ = q;
			altQ = q2;
		}
		
		targX = 50;
		targY = 50;
	}
	
	public void getPathsTo(int targX, int targY) {
		long startTime;
		if (Constants.DEBUG_ENABLED) {
			startTime = System.nanoTime();
		}
		int sourceNodeIdx = (cellsRef[targX][targY] & 0xFFF) - 1;
		Node source =  nodesRef.get(sourceNodeIdx);
//		source.distance = source.halfDist;
		source.distance = 0;
		source.tag = tag;
		source.next = source;
		curQ.decreaseKey(source.entry, source.distance);
		do {
			// get node with smallest distance
			Entry uEntry = curQ.dequeueMin();
			if (uEntry == null)
				break;
			Node u = uEntry.getValue();
			altQ.enqueue(u, Double.MAX_VALUE);
			lastDistance = u.distance;
			
			// for each neighbour v of u
			for (int i = 0; i < u.neighbours.size(); ++i) {
				Node v = u.neighbours.get(i);
				int alt = u.distance + v.halfDist;
				if ((v.tag != tag) || (alt < v.distance)) {
					v.tag = tag;
					v.distance = alt;
					v.next = u;
					curQ.decreaseKey(v.entry, v.distance);
				}
			}
		} while(true);
		
		FibonacciHeap tmp = curQ;
		curQ = altQ;
		altQ = tmp;
		tag++;
		if (Constants.DEBUG_ENABLED) {
			long endTime = System.nanoTime();
			long duration = endTime - startTime;
			Gdx.app.debug(TAG, "Time to compute computPathsDijkstra:" + (float)(duration / 1000000000.0f));
		}
	}

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			getPathsTo(targX, targY);
		}
	}
}
