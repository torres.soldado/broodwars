package com.hexperiments.broodwars.map;

import java.util.ArrayList;

import com.hexperiments.broodwars.map.FibonacciHeap.Entry;

public class Node {
	public static final String TAG = Node.class.getName(); // for debugging
	public int id;
	public int x;
	public int y;
	public int w;
	public int h;
	public int centerX;
	public int centerY;
	ArrayList<Node> neighbours;
	
	// path finding
	public int distance;
	public int halfDist;
	public Node next;
	int tag;
	public Entry entry;
	
	public Node (int x, int y, int w, int h, int id) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.id = id;
		centerX = x + w / 2;
		centerY = y + h / 2;
		neighbours = new ArrayList<Node>(5);
		
		// path finding
		halfDist = w / 2;
		distance = 0;
		next = this;
		tag = Integer.MAX_VALUE;
	}
	
	public void addNeighbour(Node neighbour) {
		if (!neighbours.contains(neighbour))
			neighbours.add(neighbour);
	}
}	
