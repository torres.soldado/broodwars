package com.hexperiments.broodwars.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;
import com.hexperiments.broodwars.Broodwars;
import com.hexperiments.broodwars.utils.Constants;


public class DesktopLauncher {
	private static boolean rebuildAtlas = false;
	private static boolean drawDebugOutline = false;
	
	public static void main (String[] arg) {
		if (rebuildAtlas) {
			Settings settings = new Settings();
			settings.maxWidth = 1024;
			settings.maxHeight = 1024;
			settings.debug = drawDebugOutline;
			TexturePacker2.process(settings, "assets-raw/images", "../android/assets/images",
					"game.pack");
		}
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Broodwars";
		config.width = (int) Constants.VIEWPORT_GUI_WIDTH;
		config.height = (int) Constants.VIEWPORT_GUI_HEIGHT;
		new LwjglApplication(new Broodwars(), config);
	}
}
